CREATE TABLE IF NOT EXISTS users
(
    id               INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    email            VARCHAR(255) NOT NULL UNIQUE,
    password         TEXT         NOT NULL,
    token            TEXT,
    token_expired_at DATETIME,
    created_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);