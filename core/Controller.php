<?php

declare(strict_types=1);

namespace Core;

use App\Enums\Http\Status;
use Core\Traits\Validator;

abstract class Controller
{
    use Validator;
    public function before(string $action, array $params = []): bool
    {
        return true;
    }

    public function after(string $action): void {}

    protected function response(Status $status, array $body = [], array $errors = []): array
    {
        return compact('status', 'body', 'errors');
    }
}