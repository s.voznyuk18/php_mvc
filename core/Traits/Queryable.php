<?php

declare(strict_types=1);

namespace Core\Traits;

use App\Enums\DB\SQL;
use Core\DB;
use PDO;

trait Queryable
{
    static protected string|null $table = null;
    static protected string $query = '';
    protected array $commands = [];

    public static function __callStatic(string $name, array $arguments)
    {
        if (in_array($name, ['where'])) {
            return call_user_func_array([new static, $name], $arguments);
        }

        throw new \Exception("Static method not allowed", 422);
    }

    public function __call(string $name, array $arguments)
    {
        if (in_array($name, ['where'])) {
            return call_user_func_array([$this, $name], $arguments);
        }

        throw new \Exception("Static method not allowed", 422);
    }

    static public function select(array $columns = ['*']): static
    {
        static::resetQuery();
        $obj = new static;
        $obj->commands[] = 'select';
        static::$query = 'SELECT ' . implode(',', $columns) . ' FROM ' . static::$table;
        return $obj;
    }

    static public function create(array $fields): null|static
    {
        $params = static::prepareQueryParams($fields);


        $query = DB::connect()->prepare(
          "INSERT INTO " . static::$table . " ($params[keys]) VALUES ($params[placeholders])"
        );

        if (!$query->execute($fields)) {
            return null;
        }

        return static::find((int)DB::connect()->lastInsertId());
    }

    static public function save(array $fields, int $id): bool
    {
        $params = static::prepareUpdateParams($fields);

        $query = DB::connect()->prepare(
          "UPDATE " . static::$table . " SET $params[placeholders] WHERE id = :id"
        );

        $fields['id'] = $id; // Add id to the fields array for binding

        return $query->execute($fields);
    }

    static public function find(int $id): static
    {
        $query = DB::connect()->prepare('SELECT * FROM ' . static::$table . ' WHERE id=:id');
        $query->bindParam('id', $id);

        if (!$query->execute()) {
            throw new \PDOException('Failed to execute query');
        }

        return $query->fetchObject(static::class);
    }

    static public function findBy(string $column, mixed $value): ?static
    {
        $query = DB::connect()->prepare('SELECT * FROM ' . static::$table . " WHERE $column = :$column");
        $query->bindParam($column, $value);

        if (!$query->execute()) {
            throw new \PDOException('Failed to execute query');
        }

        $result = $query->fetchObject(static::class);

        return $result ?: null;
    }

    protected function where(string $column, SQL $operator = SQL::EQUAL, mixed $value = null): static
    {
        $this->prevent(['order', 'limit', 'having', 'group', 'where'], 'WHERE can not be used after');
        $obj = in_array('select', $this->commands) ? $this : static::select();

        if (
          !is_null($value) &&
          !is_bool($value) &&
          !is_numeric($value) &&
          !is_array($value)
        ) {
            $value = "'$value'";
        }

        if (is_null($value)) {
            $value = SQL::NULL->value;
        }

        if (is_array($value)) {
            $value = array_map(fn($item) => is_string($item) && $item !== SQL::NULL->value ? "'$item'" : $item, $value);
            $value = '(' . implode(', ', $value) . ')';
        }

        if (!in_array('where', $obj->commands)) {
            static::$query .= " WHERE";
            $obj->commands[] = 'where';
        }

        static::$query .= " $column $operator->value $value";

        return $obj;
    }

    static protected function prepareQueryParams(array $fields): array
    {
        $keys = array_keys($fields);
        $placeholders = preg_filter('/^/', ':', $keys); // name = :name

        return [
          'keys' => implode(', ', $keys),
          'placeholders' => implode(', ', $placeholders)
        ];
    }

    static protected function prepareUpdateParams(array $fields): array
    {
        $keys = array_keys($fields);
        $placeholders = array_map(fn($key) => "$key = :$key", $keys); // key = :key

        return [
          'placeholders' => implode(', ', $placeholders)
        ];
    }

    protected function prevent(array $preventMethods, string $text = ''): void
    {
        foreach ($preventMethods as $method) {
            if (in_array($method, $this->commands)) {
                $message = sprintf(
                  "%s: %s [%s]",
                  static::class,
                  $text,
                  $method
                );
                throw new \Exception($message, 422);
            }
        }
    }

    static protected function resetQuery(): void
    {
        static::$query = '';
    }

    public function get(): array
    {
        return DB::connect()->query(static::$query)->fetchAll(PDO::FETCH_CLASS, static::class);
    }

    static public function all(array $columns = ['*']): array
    {
        return static::select($columns)->get();
    }

}