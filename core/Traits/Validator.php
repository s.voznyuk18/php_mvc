<?php

declare(strict_types=1);

namespace Core\Traits;

use App\Enums\Validation\ValidationRules;

trait Validator
{
    private array $errors = [];

    protected function validate(array $data, array $rules): bool
    {
        $parsedRules = $this->parseValidationRules($rules);

        foreach ($parsedRules as $fieldName => $fieldRules) {
            if (isset($data[$fieldName])) {
                foreach ($fieldRules as $ruleName => $ruleValue) {
                    match ($ruleName) {
                        ValidationRules::REQUIRED->value => $this->checkRequired($fieldName, $data[$fieldName]),
                        ValidationRules::EMAIL->value => $this->checkEmail($fieldName, $data[$fieldName]),
                        ValidationRules::MIN->value => $this->checkMinLength($fieldName, $ruleValue, $data[$fieldName]),
                    };
                }
            }
        }
        return empty($this->errors);
    }

    private function parseValidationRules(array $rules): array
    {
        $rulesAssoc = [];

        foreach ($rules as $fieldName => $ruleString) {
            $ruleParts = explode('|', $ruleString);
            foreach ($ruleParts as $rule) {
                if (str_contains($rule, ':')) {
                    [$key, $value] = explode(':', $rule);
                    $this->validateRule($key);
                    $rulesAssoc[$fieldName][$key] = $value;
                } else {
                    $this->validateRule($rule);
                    $rulesAssoc[$fieldName][$rule] = $rule;
                }
            }
        }
        return $rulesAssoc;
    }

    private function validateRule(string $rule): bool
    {
        if (!in_array($rule, ValidationRules::value())) {
            throw new \Exception('undefined rule');
        }
        return true;
    }

    private function checkRequired(string $fieldName, string $fieldValue): void
    {
        $fieldValue = trim($fieldValue);
        if (empty($fieldValue)) {
            $format = '%s is required/';
            $message = sprintf($format, $fieldName);
            $this->recordError($fieldName, $message);
        }
    }

    private function checkEmail(string $fieldName, string $fieldValue): void
    {
        $isEmail = filter_var($fieldValue, FILTER_VALIDATE_EMAIL);
        if (!$isEmail) {
            $format = '%s is not Email/';
            $message = sprintf($format, $fieldName);
            $this->recordError($fieldName, $message);
        }
    }

    private function checkMinLength(string $fieldName, string $ruleValue, string $fieldValue): void
    {
        if (strlen($fieldValue) < $ruleValue) {
            $format = '%s must be more than %d characters/';
            $message = sprintf($format, $fieldName, $ruleValue);
            $this->recordError($fieldName, $message);
        }
    }

    protected function recordError(string $fieldName, string $message): void
    {
        if (!isset($this->errors[$fieldName])) {
            $this->errors[$fieldName] = '';
        }

        $this->errors[$fieldName] .= $message;
    }

    protected function getErrors(): array
    {
        return $this->errors;
    }
}
