<?php

declare(strict_types=1);

namespace Core;

use Core\Traits\Queryable;

abstract class Model
{
    use Queryable;

    public int $id;

    public function toArray(): array
    {
        $reflectiobClass = new \ReflectionClass(static::class);
        $publicProperties = $reflectiobClass->getProperties(\ReflectionProperty::IS_PUBLIC);

        $data = [];

        foreach ($publicProperties as $property) {
            $propertyName = $property->getName();

            if(in_array($propertyName, ['commands', 'table'])) {
                continue;
            }

            $data[$propertyName] = $this->$propertyName;
        }
        return $data;
    }
}