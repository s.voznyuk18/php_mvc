#!/usr/local/bin/php

<?php

use App\Commands\MigrationCreate;
use App\Commands\MigrationRun;
use Dotenv\Dotenv;
use Symfony\Component\Console\Application;

const BASE_DIR = __DIR__;

require BASE_DIR . '/vendor/autoload.php';

try {
    $dotenv = Dotenv::createUnsafeImmutable(BASE_DIR);
    $dotenv->load();

    $application = new Application();
    $application->add(new MigrationCreate());
    $application->add(new MigrationRun());
    $application->run();
}  catch (Exception $e) {
    var_dump($e->getMessage());
}