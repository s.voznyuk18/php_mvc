<?php

declare(strict_types=1);

namespace App\Models;

use Core\Model;

class Folder extends Model
{
    static protected string|null $table = 'folders';
    public int $user_id;
    public string $title, $created_at, $updated_at;
}