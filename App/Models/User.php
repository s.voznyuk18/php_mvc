<?php

declare(strict_types=1);

namespace App\Models;

use Core\Model;

class User extends Model
{
    protected static ?string $table = 'users';

    public string $email, $password;
    public ?string $token, $token_expired_at, $created_at, $updated_at;
}