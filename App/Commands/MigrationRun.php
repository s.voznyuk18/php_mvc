<?php

declare(strict_types=1);

namespace App\Commands;

use Core\DB;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
  name: 'migration:run',
  description: 'Run migration.',
  aliases: ['migration:run'],
  hidden: false
)]
class MigrationRun extends Command
{
    const string MIGRATIONS_DIR = BASE_DIR . '/migrations';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {


        try {
            DB::connect()->beginTransaction();
            $output->writeln("Migration process start...");

            $this->createMigrationTable($output);
            $this->runMigrations($output);

            if (DB::connect()->inTransaction()) {
                DB::connect()->commit();
            }

            $output->writeln("Migration process is finished");
            return Command::SUCCESS;
        } catch (\PDOException $exception) {
            if (DB::connect()->inTransaction()) {
                DB::connect()->rollBack();
            }

            $output->writeln("Migration process is failure $exception");
            return Command::FAILURE;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }

    protected function createMigrationTable(OutputInterface $output): void
    {
        $output->writeln("- Run migration table query");

        $query = DB::connect()->prepare(
          "
            CREATE TABLE IF NOT EXISTS migrations (
                id INT(8) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL UNIQUE
            )"
        );

        if (!$query->execute()) {
            throw new \Exception("Smth went wrong with migration table query");
        }

        $output->writeln("- Migrations table was created!");
    }

    protected function runMigrations(OutputInterface $output): void
    {
        $output->writeln("Fetch migrations...");
        $migrations = scandir(static::MIGRATIONS_DIR);
        $migrations = array_values(array_diff($migrations, ['.', '..']));

        $handledMigrations = $this->getHandledMigrations();

        if (!empty($migrations)) {
            foreach ($migrations as $migration) {
                $output->writeln("-run $migration");
                if (in_array($migration, $handledMigrations)) {
                    $output->writeln("--skip $migration");
                    continue;
                }
                $sql = file_get_contents(static::MIGRATIONS_DIR . "/$migration");
                $query = DB::connect()->prepare($sql);

                if ($query->execute()) {
                    $this->createMigrationRecord($migration);
                    $output->writeln("- `$migration` migrated");
                }
            }
        }
    }

    protected function createMigrationRecord(string $migration): void
    {
        $query = DB::connect()->prepare("INSERT INTO migrations (name) VALUES (:name)");
        $query->bindParam('name', $migration);
        $query->execute();
    }

    protected function getHandledMigrations(): array
    {
        $query = DB::connect()->prepare("SELECT name FROM migrations");
        $query->execute();
        return array_map(fn($item) => $item['name'], $query->fetchAll());
    }
}