<?php

declare(strict_types=1);

namespace App\Commands;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

#[AsCommand(
  name: 'migration:create',
  description: 'Creates a new migration.',
  aliases: ['migration:create'],
  hidden: false
)]
class MigrationCreate extends Command
{
    const string MIGRATIONS_DIR = BASE_DIR . '/migrations';

    protected function configure(): void
    {
        $this->addArgument('name', InputArgument::REQUIRED, 'The name of the migration.');
    }

    protected function createDir(): void
    {
        if (!file_exists(static::MIGRATIONS_DIR)) {
            mkdir(static::MIGRATIONS_DIR);
        }
    }

    protected function createMigration(InputInterface $input, OutputInterface $output): void
    {
        $name = time() . '_' . $input->getArgument('name');
        $fullPath = static::MIGRATIONS_DIR . "/$name.sql";

        file_put_contents($fullPath, '', FILE_APPEND);

        $output->writeln("File was successfully created!");
        $output->writeln("File: $fullPath");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->createDir();
            $this->createMigration($input, $output);
            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $output->writeln("An error occurred: " . $exception->getMessage());
            return Command::FAILURE;
        }
    }
}