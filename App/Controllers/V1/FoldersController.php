<?php

declare(strict_types=1);

namespace App\Controllers\V1;

use App\Controllers\BaseApiController;
use App\Enums\DB\SQL;
use App\Enums\Http\Status;
use App\Models\Folder;

class FoldersController extends BaseApiController
{
    public function index(): array
    {
        return $this->response(Status::OK, Folder::select(['user_id', 'title'])->where('user_id', SQL::EQUAL, 1)->get());
//        return $this->response(Status::OK, Folder::where('id', SQL::EQUAL, 3)->get());
//        return $this->response(Status::OK, Folder::findBy('title', 'folder 1')->toArray());
//        return $this->response(Status::OK, Folder::find(2)->toArray());
//        return $this->response(Status::OK, Folder::all());
    }
}