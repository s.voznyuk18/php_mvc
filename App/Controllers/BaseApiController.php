<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Models\User;
use Core\Controller;
use ReallySimpleJWT\Token;

class BaseApiController extends Controller
{
    public function before(string $action, array $params = []): bool
    {
        $headers = getallheaders();
        if (!isset($headers['Authorization'])) {
            throw new \Exception('Authorization header not found', 401);
        }

        $authorizationHeader = $headers['Authorization'];
        $token = substr($authorizationHeader, 7);

        $user = User::findBy('token', $token);

        if($user !== null) {
            if (!Token::validate($user->token, $user->password)) {
                throw new \Exception('Token is invalid', 401);
            }

            if(!Token::validateExpiration($token)) {
                throw new \Exception('Token has expired', 401);
            }
        }
        return true;
    }
}