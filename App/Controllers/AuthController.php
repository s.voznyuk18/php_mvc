<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Enums\Http\Status;
use App\Models\User;
use Core\Controller;
use ReallySimpleJWT\Token;

class AuthController extends Controller
{
    public function register(): array
    {
        $fields = requestBody();

        $this->validate($fields, ["email" => 'required|email', "password" => 'required|min:3']);

        if (empty($this->getErrors())) {
            $user = User::findBy('email', $fields['email']);

            if ($user !== null) {
                return $this->response(Status::UNPROCESSABLE_CONTENT, $fields, ['email' => 'Email already exists']);
            }

            $user = User::create([
              ...$fields,
              'password' => password_hash($fields['password'], PASSWORD_BCRYPT)
            ]);

            return $this->response(Status::OK, $user->toArray());
        }

        return $this->response(Status::UNPROCESSABLE_CONTENT, $fields, $this->getErrors());
    }

    public function auth(): array
    {
        $fields = requestBody();

        $this->validate($fields, ["email" => 'required|email', "password" => 'required|min:3']);

        if (empty($this->getErrors())) {
            $user = User::findBy('email', $fields['email']);
            if ($user !== null && password_verify($fields['password'], $user->password)) {
                $expiration = time() + 3600;
                $expirationDateTime = date('Y-m-d H:i:s', $expiration);
                $token = Token::create($user->id, $user->password, $expiration, 'localhost');

                User::save(['token' => $token, 'token_expired_at' => $expirationDateTime], $user->id);

                return $this->response(Status::OK, compact('token'));
            }
        }

        return $this->response(Status::UNPROCESSABLE_CONTENT, errors: $this->getErrors());
    }
}