<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Enums\Http\Status;
use Core\Controller;

class TestController extends Controller
{
    public function index(int $id, int $uid): array
    {
        return $this->response(Status::OK, compact($id, $uid));
    }
}